
function getPhoneNumbers(text) {
	var regex = new RegExp("\\+?\\(?\\d*\\)? ?\\(?\\d+\\)?\\d*([\\s./-]\\d{2,})+","g");
	var phoneNumbers = [];
    while (match = regex.exec(text)) {
        phoneNumbers.push(match[0]);
    }
    return phoneNumbers;
}

function getEmails(text) {
	var regex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;
	var emails = [];
    while (match = regex.exec(text)) {
        emails.push(match[0]);
    }
    return emails;
}

function getFirstAndNameLastName(text) {
	var lines = text.split(/\r\n|\r|\n/);
	var result = lines[0].split(/\s+/);
    return result;
}

document.getElementById('person_background').addEventListener('change', function() {
	var fullText = document.getElementById('person_background').value;
	var phoneNumbers = getPhoneNumbers(fullText);
	document.getElementById("person_contact_data_phone_numbers__number").value = phoneNumbers[0];
	var emails = getEmails(fullText);
	if (emails){
		document.getElementById("person_contact_data_email_addresses__address").value = emails[0];
	}
	var firstAndLast = getFirstAndNameLastName(fullText);
	document.getElementById("person_first_name").value = firstAndLast[0];
	document.getElementById("person_last_name").value = firstAndLast[1];
});