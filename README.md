# Chrome Extension to Easily Add Contacts to HighRise CRM

This is a chrome extension that lets you easily add contacts from signature to the [Highrise CRM](http://highrisehq.com).

To install this:

 * download the files to your computer
 * in Chrome, go to Window -> Extensions, make sure "Developer mode" is checked, and click "load unpacked extension"
 * choose the folder where you downloaded the code

 To use it:

 * open the add contact page.
 * paste any signature code in the last field (background info)

 How does this work :
 
 * First and Last Name are extracted from the first line
 * First Phone number will be added in the right field but you need to click it as it is hidden
 * same for email

License: GNU Public License, [V3: http://www.opensource.org/licenses/GPL-3.0](http://www.opensource.org/licenses/GPL-3.0)

